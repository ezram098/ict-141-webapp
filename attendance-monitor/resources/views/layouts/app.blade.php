<!doctype html>
<html lang="{{'config'}}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name','Attendance-Monitor')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}"  >
        <!-- Styles -->

    </head>
    <body>
        @yield('content')
    </body>
</html>
