<?php
session_start();
error_reporting(0);
require 'internsession.php';
?>
<?php
include('header.php');

require 'dbconnection.php';
?>



    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4 text-center text-lg-left">Announcements</h1>

      <div class="row text-center text-lg-left">
<?php

// require 'internattendance/dbconnection.php';
$getalluser = "
SELECT *
FROM post
WHERE isdeactivated =  0
ORDER BY 
createddate
DESC
";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    if(mysqli_num_rows($outputresult) > 0){
        while($row = mysqli_fetch_array($outputresult)){
?>
         <div class="col-xl-6 col-sm-8 mb-6" style="padding-bottom: 30px !important;">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-shopping-cart"></i>
                  </div>
                  <img style="width: 500px !important;
    height: 300px !important;" src= "<?php echo '../internattendance' .$row['photo'] ?>" >
                  <div class="mr-5"><?php echo $row['name'] ?></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                  <span class="float-left"><?php echo $row['description'] ?></span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
<?php
        }
    }
}
?>
    </div>
    <br><br>