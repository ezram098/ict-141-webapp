<?php
session_start();
error_reporting(0);
require 'internsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

 <!-- DataTables Example -->
 <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-envelope"></i>
              Messages &nbsp;&nbsp;
              </div>
            <div class="card-body">
              <div class="table-responsive">
<?php
require 'dbconnection.php';
$person_id = (int)$_SESSION['person_id'];
 $getalluser = "
 SELECT * FROM message_person WHERE person_id = $person_id 
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    if(mysqli_num_rows($outputresult) > 0){
?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Message ID</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Sent Date</th>
                      <th>Reply</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
            while($row = mysqli_fetch_array($outputresult)){
    
                echo "<tr>";
                echo "<td>" .$row['messageperson_id']."&nbsp;";
                echo "<td>" .$row['title']."</td>";
                echo "<td>" .$row['description']."</td>";
                echo "<td>" .$row['created']."</td>";
                echo "<td>" .$row['reply']."</td>";
                echo "</tr>";
        }
    }
}?>
                   
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div>


<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 