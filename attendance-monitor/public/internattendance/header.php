<?php
session_start();
error_reporting(0);
if($_SESSION['usertype'] == 'admin')
{
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Attendance Tracker - Login</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="shortcut icon" href="logo.png" />
        <link href="../css/thumbnail-gallery.css" rel="stylesheet"> 
    <script src="../vendor/jquery/jquery.min.js"></script>

  </head>
  <body id="page-top">

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

  <a class="navbar-brand mr-1" href="index.html">ATTENDANCE TRACKER MANAGEMENT</a>

  <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
    <i class="fas fa-bars"></i>
  </button>

  <!-- Navbar Search -->
  <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
    
  </form>

  <!-- Navbar -->
  <ul class="navbar-nav ml-auto ml-md-0">
    <li class="nav-item dropdown no-arrow mx-1">
      <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-envelope fa-fw"></i>
        <span class="badge badge-danger"><?php
require 'dbconnection.php';
 $getalluser = "
 SELECT COUNT(*) FROM message_person WHERE isread = 0
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    while ($row = mysqli_fetch_array($outputresult)){
    echo $row['COUNT(*)'];
    }
}
?></span>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
        <a class="dropdown-item" href="adminmanagemessageslist.php">View Details</a>
      </div>
    </li>
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user-circle fa-fw"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
      </div>
    </li>
  </ul>

</nav>

<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="userhomepage.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="adminmanageuser.php">
        <i class="fas fa-fw fa-user"></i>
        <span>Manage Users</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="adminaddpostlist.php">
        <i class="fas fa-fw fa-user"></i>
        <span>Manage Post</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="manage_attendance.php">
        <i class="fas fa-fw fa-user"></i>
        <span>Manage Attendace</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="adminschedulelist.php">
        <i class="fas fa-fw fa-user"></i>
        <span>Manage Schedules</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="adminmanagemessageslist.php">
        <i class="fas fa-fw fa-comments"></i>
        <span>Manage Messages</span></a>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">
<?php
}
else if($_SESSION['usertype'] == 'intern')
{

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Attendance Tracker - Login</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="shortcut icon" href="logo.png" />

  </head>
  <body id="page-top">

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

  <a class="navbar-brand mr-1" href="index.html">ATTENDANCE TRACKER MANAGEMENT</a>

  <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
    <i class="fas fa-bars"></i>
  </button>

  <!-- Navbar Search -->
  <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
   
  </form>

  <!-- Navbar -->
  <ul class="navbar-nav ml-auto ml-md-0">
    <li class="nav-item dropdown no-arrow mx-1">
      <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-envelope fa-fw"></i>
        <span class="badge badge-danger"><?php
require 'dbconnection.php';
$serssionPersonId =  $_SESSION['person_id'];
 $getalluser = "
 SELECT COUNT(*) FROM message_person WHERE person_id = $serssionPersonId 
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    while ($row = mysqli_fetch_array($outputresult)){
    echo $row['COUNT(*)'];
    }
}
?></span>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
        <a class="dropdown-item" href="internmanagemessages.php">View Details</a>
      </div>
    </li>
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user-circle fa-fw"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
               <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
      </div>
    </li>
  </ul>

</nav>

<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="internhomepage.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
     <li class="nav-item">
      <a class="nav-link" href="internviewannouncements.php">
        <i class="fas fa-fw fa-envelope"></i>
        <span>View Announcement</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="internmessageadmin.php">
        <i class="fas fa-fw fa-envelope"></i>
        <span>Message Admin</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="internmanagemessages.php">
        <i class="fas fa-fw fa-envelope"></i>
        <span>Manage Messages</span></a>
    </li>
  
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

<?php

}
?>
   