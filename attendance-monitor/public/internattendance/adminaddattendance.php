<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
$idtoedit =$_GET['id'];
require 'dbconnection.php';
   
$queryforedituser = mysqli_query($databaseconnection,"SELECT * FROM person WHERE person_id =$idtoedit");
$fetch = mysqli_fetch_assoc($queryforedituser);
?>
<!-- CONTENT HERE START-->

<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Create new schedule for <b><?php echo $fetch['fname'] ?> <?php echo $fetch['lname'] ?></b> </div>
        <div class="card-body">
          <form method="post" action="adminaddattendanceprocess.php" >
          <input type="number" name="id" hidden value="<?php echo $fetch['person_id']; ?>">
          <input type="number" name="schedid" hidden value="<?php echo $fetch['sched_id']; ?>">
          <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    Date
                    <input type="date" name="datein" id="username" class="form-control" placeholder="Date" required="required">
                  </div>
                </div>
<!--                 <div class="col-md-6">
                  <div class="form-label-group">
                    Time
                    <input type="time" name="timein" id="username" class="form-control" placeholder="Time" required="required">
                  </div>
                </div> -->
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                  Remarks
                    <textarea type="text" name="remarks" id="fname" class="form-control"  autofocus="autofocus"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <button class="btn btn-primary btn-block" name="submit" type="submit" value="1">Present</a>
            <button class="btn btn-danger btn-block" name="submit" type="submit" value="0">Absent</a>
          </form>
        </div>
      </div>
    </div>

<!--CONTENT HERE END -->