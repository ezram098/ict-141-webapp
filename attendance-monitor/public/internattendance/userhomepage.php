<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->
<div id="content-wrapper">

<div class="container-fluid">

  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Overview</li>
  </ol>

  <!-- Icon Cards-->
  <div class="row">
    <div class="col-xl-3 col-sm-6 mb-3">
      <div class="card text-white bg-primary o-hidden h-100">
        <div class="card-body">
          <div class="card-body-icon">
            <i class="fas fa-fw fa-comments"></i>
          </div>
          <div class="mr-5">
<?php
require 'dbconnection.php';
 $getalluser = "
 SELECT COUNT(*) FROM message_person WHERE isread = 0
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    while ($row = mysqli_fetch_array($outputresult)){
    echo $row['COUNT(*)'];
    }
}
?>
          New Message(s)!</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="adminmanagemessageslist.php">
          <span class="float-left">View Details</span>
          <span class="float-right">
            <i class="fas fa-angle-right"></i>
          </span>
        </a>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
      <div class="card text-white bg-warning o-hidden h-100">
        <div class="card-body">
          <div class="card-body-icon">
            <i class="fas fa-fw fa-list"></i>
          </div>
          <div class="mr-5">
          <?php
require 'dbconnection.php';
 $getalluser = "
 SELECT COUNT(*) FROM person WHERE usertype = 'intern'
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    while ($row = mysqli_fetch_array($outputresult)){
    echo $row['COUNT(*)'];
    }
}
?>
          Intern(s)!</div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
      <div class="card text-white bg-success o-hidden h-100">
        <div class="card-body">
          <div class="card-body-icon">
            <i class="fas fa-fw fa-shopping-cart"></i>
          </div>
          <div class="mr-5">    <?php
require 'dbconnection.php';
 $getalluser = "
 SELECT COUNT(*) FROM person
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    while ($row = mysqli_fetch_array($outputresult)){
    echo $row['COUNT(*)'];
    }
}
?> Users</div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
      <div class="card text-white bg-danger o-hidden h-100">
        <div class="card-body">
          <div class="card-body-icon">
            <i class="fas fa-fw fa-life-ring"></i>
          </div>
          <div class="mr-5"> <?php
require 'dbconnection.php';
 $getalluser = "
 SELECT COUNT(*) FROM person WHERE islocked = 1
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    while ($row = mysqli_fetch_array($outputresult)){
    echo $row['COUNT(*)'];
    }
}
?> Locked Users</div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>

<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 