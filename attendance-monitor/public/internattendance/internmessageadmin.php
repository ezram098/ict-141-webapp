<?php
session_start();
error_reporting(0);
require 'internsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->
<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-body">
          <form method="post" action="internmessageadminprocess.php">
          <input type="text" hidden name="previousmessage">
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group"> Title : 
                    <input type="text" name="title" class="form-control" autocomplete="off"> </input>
                  </div>
                  <div class="form-label-group"> Body : 
                    <textarea type="text" name="body" class="form-control"> </textarea>
                  </div>
                </div>
              </div>
            </div>
            <button class="btn btn-primary btn-block" name="submit" type="submit">Send Message</a>
          </form>
        </div>
      </div>
    </div>

<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 