<?php
session_start();
error_reporting(0);
require 'adminsession.php';
$curdate = time();
$curdate = date("y-m-d h:i",$curdate);
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

 <!-- DataTables Example -->
 <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Manage Schedule &nbsp;&nbsp;
              <i class="fas fa-plus"></i>
              <a href="adminscheduleadd.php"> Add Schedule</a>
              </div>
            <div class="card-body">
              <div class="table-responsive">
<?php
require 'dbconnection.php';
 $getalluser = "
 SELECT *
 FROM schedule sc
 -- JOIN person per ON sc.person_id = per.person_id
 -- JOIN schedule sc ON at.sched_id = at.sched_id
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    if(mysqli_num_rows($outputresult) > 0){
?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Schedule Reference #</th>
                      <th>time in</th>
                      <th>time out</th>
                      <th>Remarks</th>
                      <th>Edit</th>
                      <th>Manage</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
            while($row = mysqli_fetch_array($outputresult)){
                echo "<tr>";
                echo "<td>" .$row['sched_id']."</td>";
                echo "<td>" .date("h:i A", strtotime($row['time_in']))."</td>";  
                echo "<td>" .date("h:i A", strtotime($row['time_out']))."</td>";
                echo "<td>" .$row['remarks']."</td>";

                echo "<td> 
                <a href='adminscheduleedit.php?id={$row['sched_id']}'><i class='fas fa-pen'></i></a>
                &nbsp;
                </td>";
                echo $row['lockedSched'] ? "<td><a href='adminactivatesch.php?id={$row['sched_id']}'><i class='fas fa-lock'></i></a></td>" : "<td><a href='admindeactivatesch.php?id={$row['sched_id']}'><i class='fas fa-lock-open'></i></a></td>";
                echo "</tr>";
        }
    }
}?>
                   
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Last updated at <?php echo $curdate; ?></div>
          </div>


<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 