<?php
session_start();
error_reporting(0);
require 'adminsession.php';
$curdate = time();
$curdate = date("y-m-d h:i",$curdate);
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

 <!-- DataTables Example -->
 <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Manage Posts &nbsp;&nbsp;
              <i class="fas fa-plus"></i>
              <a href="adminaddpost.php"> Add Post </a>
              </div>
            <div class="card-body">
              <div class="table-responsive">
<?php
require 'dbconnection.php';
 $getalluser = "
 SELECT *
 FROM post po
 JOIN person ps ON po.person_id = ps.person_id
 ORDER BY
 po.createddate
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    if(mysqli_num_rows($outputresult) > 0){
?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Post Reference #</th>
                      <th>Title</th>
                      <th>Description / Body</th>
                      <th>Photo</th>
                      <th>Created By</th>
                      <th>Created Date</th>
                      <th>Edit</th>
                      <th>Deactivate</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
            while($row = mysqli_fetch_array($outputresult)){
                echo "<tr>";
                echo "<td>" .$row['post_id']."</td>";
                echo "<td>" .$row['name']."</td>";  
                echo "<td>" .$row['description']."</td>";
                ?>
                <td><img style="width: 100px !;
    height: 70px !important;" src="<?php echo "internattendance/.." .$row['photo'];  ?>"></td>

                <?php
                echo "<td>" .$row['fname']."&nbsp;".$row['lname']."</td>";
                echo "<td>" .$row['createddate']."</td>";
                echo "<td> 
                <a href='admineditpost.php?id={$row['post_id']}'><i class='fas fa-pen'></i></a>
                &nbsp;
                </td>";
                echo $row['isdeactivated'] ? "<td><a href='adminactivatepost.php?id={$row['post_id']}'><i class='fas fa-lock'></i></a></td>" : "<td><a href='admindeactivatepost.php?id={$row['post_id']}'><i class='fas fa-lock-open'></i></a></td>";
                echo "</tr>";
        }
    }
}?>
                   
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Last updated at <?php echo $curdate; ?></div>
          </div>


<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 