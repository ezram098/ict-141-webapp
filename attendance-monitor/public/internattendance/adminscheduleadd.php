<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Create new schedule</div>
        <div class="card-body">
          <form method="post" action="adminscheduleaddprocess.php" >
          <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    Time In
                    <input type="time" name="timein" id="username" class="form-control" placeholder="Announcement Title" required="required">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    Time Out
                    <input type="time" name="timeout" id="username" class="form-control" placeholder="Announcement Title" required="required">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                  Remarks
                    <textarea type="text" name="remarks" id="fname" class="form-control" required="required" autofocus="autofocus"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <button class="btn btn-primary btn-block" name="submit" type="submit">Create Attendance</a>
          </form>
        </div>
      </div>
    </div>

<!--CONTENT HERE END -->