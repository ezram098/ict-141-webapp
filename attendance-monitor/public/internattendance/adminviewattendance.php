<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

 <!-- DataTables Example -->
 <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Manage Attendance &nbsp;&nbsp;
              </div>
            <div class="card-body">
              <div class="table-responsive">
<?php

require 'dbconnection.php';
 $getalluser = "
 SELECT *
 FROM attendance at
 JOIN person per ON at.person_id = per.person_id
 JOIN schedule sc ON at.sched_id = at.sched_id
 where date='$_GET[date]'
 GROUP BY
 at.attendance_id
 ORDER BY   
 date
 DESC 
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    if(mysqli_num_rows($outputresult) > 0){
?>

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Attendance Reference #</th>
                       <th>Name </th>
                      <th>Date</th>
                      <th>Status</th>
                      <th>Schedule Time</th>
                      <th>Remarks</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
            while($row = mysqli_fetch_array($outputresult)){
                echo "<tr>";
                echo "<td>" .$row['attendance_id']."</td>";
                echo "<td>" .$row['fname']." ".$row['lname']."</td>";
                echo "<td>" .$row['date']."</td>";
                echo  $row['isinisout'] ? "<td>Present</td>" : "<td>Absent</td>";
                echo "<td>" .date("h:i A", strtotime($row['time_in'])). "-" .date("h:i A", strtotime($row['time_out']))."</td>";  
                echo "<td>" .$row['remarksattendance']."</td>";


                // echo "<input type='hidden' name='date' value='" .$row['date']."' >";
                echo "<td><a href='admindeleteattendance.php?id=".$row['attendance_id']."'><i class='fas fa-trash'></i></a></td>";
                echo "</tr>";
        }
    }
}?>
                   
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div>


<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 