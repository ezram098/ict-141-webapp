<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->
<?php
     $idtoedit =$_GET['id'];
     require 'dbconnection.php';
        
$queryforedituser = mysqli_query($databaseconnection,"SELECT * FROM person WHERE person_id =$idtoedit");
$fetch = mysqli_fetch_assoc($queryforedituser);
?>
<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Edit a User</div>
        <div class="card-body">
          <form method="post" action="registerprocessedit.php">
          <input type="hidden" name="id" value="<?php echo $fetch['person_id']; ?>">
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" value="<?php echo $fetch['fname']; ?>" name="fname" id="fname" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                    <label for="fname">First name</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" value="<?php echo $fetch['lname']; ?>" name="lname" id="lname" class="form-control" placeholder="Last name" required="required">
                    <label for="lname">Last name</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="date" value="<?php echo $fetch['bdate']; ?>" name="bdate" id="bdate" class="form-control" placeholder="Birthday" required="required">
                <label for="bdate">Birthday</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                    <input type="text" value="<?php echo $fetch['contact']; ?>" name="contact" id="contact" class="form-control" placeholder="Contact Number" required="required" autofocus="autofocus">
                    <label for="contact">Contact Number</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                    <input type="text" value="<?php echo $fetch['email']; ?>" name="email" id="email" class="form-control" placeholder="Email" required="required" autofocus="autofocus">
                    <label for="email">Email</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                    <input type="text" value="<?php echo $fetch['course']; ?>" name="course" id="course" class="form-control" placeholder="Course" required="required" autofocus="autofocus">
                    <label for="course">Course</label>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" value="<?php echo $fetch['username']; ?>" name="username" id="inputPasswordu" class="form-control" placeholder="User Name" required="required">
                    <label for="inputPasswordu">Username</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" value="<?php echo $fetch['password']; ?>" name="password" id="inputPassword" class="form-control" placeholder="Password" required="required">
                    <label for="inputPassword">Password</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
              <select name="usertype"id="utype" class="form-control m-input">
                                                <option value="1">
                                                Intern
                                                </option>
                                                <option value="2">
                                                Admin
                                                </option>
              </select>
              </div>
            </div>
                        <div class="form-group">
<!--               <div class="form-label-group">
              <select name="sched_id"id="utype" class="form-control m-input">
                                                <option value="1">
                                                1
                                                </option>
                                                <option value="2">
                                                2
                                                </option>
                                                <option value="3">
                                                3
                                                </option>
              </select>
              </div> -->
            </div>
            <button class="btn btn-primary btn-block" name="submit" type="submit">Update</a>
          </form>
        </div>
      </div>
    </div>

<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 