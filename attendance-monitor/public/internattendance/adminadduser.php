<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Register an Account</div>
        <div class="card-body">
          <form method="post" action="registerprocess.php">
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" name="fname" id="fname" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                    <label for="fname">First name</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" name="lname" id="lname" class="form-control" placeholder="Last name" required="required">
                    <label for="lname">Last name</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="date" name="bdate" id="bdate" class="form-control" placeholder="Birthday" required="required">
                <label for="bdate">Birthday</label>
              </div>
            </div>

            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                    <input type="text" name="contact" id="contact" class="form-control" placeholder="Contact Number" required="required">
                    <label for="contact">Contact Number</label>
                  </div>
                </div>
            </div>
            <br>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required">
                    <label for="email">Email</label>
                  </div>
                </div>
            </div>
            <br>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                    <input type="text" name="course" id="course" class="form-control" placeholder="Course" required="required">
                    <label for="course">Course</label>
                  </div>
                </div>
            </div>
            <br>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" name="username" id="username" class="form-control" placeholder="User Name" required="required">
                    <label for="username">Username</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="required">
                    <label for="password">Password</label>
                  </div>
                </div>
              </div>
            </div>
            <button class="btn btn-primary btn-block" name="submit" type="submit">Register</a>
          </form>
        </div>
      </div>
    </div>

<!--CONTENT HERE END -->