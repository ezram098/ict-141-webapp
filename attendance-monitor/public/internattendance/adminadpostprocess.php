<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
if(isset($_POST['submit']))
{
    require 'dbconnection.php';
    $posttitle=$_POST['posttitle'];
    $postvalue=$_POST['postvalue'];
    $modified_date=date("Y/m/d");
    $creatorId = (int)$_SESSION['person_id'];


    $logsDesc =  "INSERT:" .$posttitle. "," .$postvalue. "," .$creatorId. "," .$modified_date;
   
    $currentDir = getcwd();
    $uploadDirectory =  "/fileuploads/";

    $errors = []; // Store all foreseen and unforseen errors here

    $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions

    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileTmpName  = $_FILES['myfile']['tmp_name'];
    $fileType = $_FILES['myfile']['type'];
    $fileExtension = strtolower(end(explode('.',$fileName)));

    $uploadPath = $currentDir . $uploadDirectory . basename($fileName); 

    $uploadPathToInsert = $uploadDirectory. basename($fileName);

    if (isset($_POST['submit'])) {

        if (! in_array($fileExtension,$fileExtensions)) {
            $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
        }

        if ($fileSize > 2000000) {
            $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
        }

        if (empty($errors)) {
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

            if ($didUpload) {
                echo "The file " . basename($fileName) . " has been uploaded";
            } else {
                echo "An error occurred somewhere. Try again or contact the admin";
            }
        } else {
            foreach ($errors as $error) {
                echo $error . "These are the errors" . "\n";
            }
        }
    }
    // Check if image file is a actual image or fake image


    $valuetoreturn = mysqli_query(
        $databaseconnection,
        "INSERT INTO post(
        name,
        photo,
        description,
        createdby,
        createddate,
        person_id
        )
         VALUES
        ('".$posttitle."',
        '".$uploadPathToInsert."',
        '".$postvalue."',
        '".$creatorId."',
        '".$modified_date."',
        '".$creatorId."'
        )");
        $logs = mysqli_query(
            $databaseconnection,
            "INSERT INTO logs(
            person_id,
            description
            )
             VALUES
            ('".$creatorId."',
            '".$logsDesc."'
            )");
    if($valuetoreturn && $logs){
        echo "<script>alert('Post has been created.')</script>";
    ?>
     <meta http-equiv="refresh" content="1;URL='adminaddpostlist.php'" />
     <?php
    }else{
        echo "Something went wrong!";
        exit();
    }
}
else
{
    echo "<script>alert('error in connection')</script>";
}
?>