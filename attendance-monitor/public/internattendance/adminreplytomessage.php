<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->
<?php
     $idtoedit =$_GET['id'];
     require 'dbconnection.php';
        
$queryforedituser = mysqli_query($databaseconnection,"SELECT * FROM message_person WHERE messageperson_id =$idtoedit");
$fetch = mysqli_fetch_assoc($queryforedituser);
?>
  <div class="container">
        <div class="card card-register mx-auto mt-5">
          <div class="card-header">Replying to : <?php echo $fetch['title']; ?></div>
          <div class="card-body">
            <form method="post" action="adminreplytomessageproccess.php">
            <input type="text" hidden name="id" value="<?php echo $idtoedit; ?>">
            <input type="text" hidden name="previousmessage" value="<?php echo $fetch['reply']; ?>">
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-12">
                    <div class="form-label-group"> Reply : 
                      <textarea type="text" name="reply" class="form-control"> </textarea>
                    </div>
                  </div>
                </div>
              </div>
              <button class="btn btn-primary btn-block" name="submit" type="submit">Reply</a>
            </form>
          </div>
        </div>
      </div>

<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 