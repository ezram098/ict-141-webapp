<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
if(isset($_POST['submit']))  
{
    $idToUpdate = $_POST['id'];
    $posttitle = $_POST['posttitle'];
    $postvalue=$_POST['postvalue'];
    $modified_date=date("Y/m/d");
    $uploadPathToInsert = $_POST['edituploadpathhidden'];
    $creatorId = (int)$_SESSION['person_id'];
    $logsDesc =  "UPDATE: " .$posttitle. "," .$postvalue. "," .$modified_date. "," .$creatorId;
   
    require 'dbconnection.php';

    $currentDir = getcwd();
    $uploadDirectory =  "/fileuploads/";

    $errors = []; // Store all foreseen and unforseen errors here

    $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
  
    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileTmpName  = $_FILES['myfile']['tmp_name'];
    $fileType = $_FILES['myfile']['type'];
    $fileExtension = strtolower(end(explode('.',$fileName)));

    $uploadPath = $currentDir . $uploadDirectory . basename($fileName); 

    if(in_array($fileExtension,$fileExtensions))
    {
       
    
        $uploadPathToInsert = $uploadDirectory. basename($fileName);
    
      
    
            if (! in_array($fileExtension,$fileExtensions)) {
                $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
            }
    
            if ($fileSize > 2000000) {
                $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
            }
    
            if (empty($errors)) {
                $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
    
                if ($didUpload) {
                    echo "The file " . basename($fileName) . " has been uploaded";
                } else {
                    echo "An error occurred somewhere. Try again or contact the admin";
                }
            } else {
                foreach ($errors as $error) {
                    echo $error . "These are the errors" . "\n";
                }
            }
        
        // Check if image file is a actual image or fake image
    }
  

    $logs = mysqli_query(
        $databaseconnection,
        "INSERT INTO logs(
        person_id,
        description
        )
         VALUES
        ('".$creatorId."',
        '".$logsDesc."'
        )");
    $sql = "UPDATE post set 
               name =  '$posttitle',
               description = '$postvalue',
               photo = '$uploadPathToInsert'
                WHERE  
                post_id = $idToUpdate
         ";

      if ($databaseconnection->query($sql) === TRUE) {
        echo "<script>alert('Post has been updated.')</script>";

        ?>
        <meta http-equiv="refresh" content="0;URL='adminaddpostlist.php'" /> 
        <?php
    } else {
        echo "<script>alert('Error On Updating')</script>";

    }
}
?>