<?php
session_start();
error_reporting(0);
require 'adminsession.php';
$curdate = time();
$curdate = date("y-m-d h:i",$curdate);
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

 <!-- DataTables Example -->
 <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Users &nbsp;&nbsp;
              <i class="fas fa-plus"></i>
              <a href="adminadduser.php"> Add User </a>
              </div>
            <div class="card-body">
              <div class="table-responsive">
<?php
require 'dbconnection.php';
 $getalluser = "
 SELECT *
 FROM person
 ORDER BY
 lname
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    if(mysqli_num_rows($outputresult) > 0){
?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>User Type </th>
                      <th>Contact</th>
                      <th>Email</th>
                      <th>Bdate</th>
                      <th>Course</th>
                      <th>Username</th>
                      <th>Is Locked</th>
                      <th>Manage</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
            while($row = mysqli_fetch_array($outputresult)){
    
                echo "<tr>";
                echo "<td>" .$row['fname']."&nbsp;".$row['lname']."</td>";
                echo "<td>" .$row['usertype']."</td>";
                echo "<td>" .$row['contact']."</td>";
                echo "<td>" .$row['email']."</td>";
                echo "<td>" .$row['bdate']."</td>";
                echo "<td>" .$row['course']."</td>";
                echo "<td>" .$row['username']."</td>";
                echo $row['islocked'] ? "<td><a href='adminunlockuser.php?id={$row['person_id']}'><i class='fas fa-lock'></i></a></td>" : "<td><a href='adminlockuser.php?id={$row['person_id']}'><i class='fas fa-lock-open'></i></a></td>";
                echo "<td> <a href='adminedituser.php?id={$row['person_id']}'><i class='fas fa-pen'></i></a>
                &nbsp;
                <a href='adminaddattendance.php?id={$row['person_id']}'><i class='fas fa-clock'></i></a>
                &nbsp;
                <a href='adminviewspecificattendance.php?id={$row['person_id']}'><i class='fas fa-eye'></i></a>
                </td>";
                echo "</tr>";
        }
    }
}?>
                   
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Last updated at <?php echo $curdate; ?></div>
          </div>


<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 