<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');

require 'dbconnection.php';
?>



<div class="container">
<div class="card panel-default">
<div class="card-header">
<div class="row">
	<div class="col">
		 	 <!-- <input type="button" value="Back" class="btn btn-info pull right" onclick="window.history.back()"> -->
	</div>
	<div class="col">
		 	<h2> <div class="well text-center">DATE:<?php echo date ("m-d-y"); ?></div> </h2>
	</div>
	<div class="col">
	 <!-- <a href="mark_attendance.php" class="btn btn-success" style="float: right;">Mark Attendance</a> -->
	</div>
	</div>


</div>
<div class="card-body">
	<!-- <form  action="show_attendance.php" method="post"> -->
		<table class="table table-striped table-bordered table-hover ">
			<thead >
			<tr>
			<th>Number</th><th>Date</th><th width="30%;">Action</th>
			</tr>
			</thead>
			<?php $result=mysqli_query($databaseconnection,"select distinct date from attendance");
			$num=0;
			while ($row=mysqli_fetch_array($result)) {
			$num++;
			
			?>
			<tr>
			<td><?php echo $num;?></td>
			<td><?php echo $row['date'];?></td>
			<td>
				<form action="adminviewattendance.php">
					
					<input type="hidden" name="date" value="<?php echo $row['date'] ?>" >
					<center><input type="submit" name="" value="View Attendance" class="btn btn-sm btn-primary"></center>
				</form>

			</td>
			<?php

			}
			?>
			</tr>
		</table>
	<!-- </form> -->
</div>
</div>
</div>
</div>