<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">POST AN Announcement</div>
        <div class="card-body">
          <form method="post" action="adminadpostprocess.php" enctype="multipart/form-data">
          <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                  Announcement Title
                    <input type="text" name="posttitle" id="username" class="form-control" placeholder="Announcement Title" required="required">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                  Announcement Message 
                    <textarea type="text" name="postvalue" id="fname" class="form-control" placeholder="Announcement Message" required="required" autofocus="autofocus">
                    </textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
            Select image to upload:
             <input type="file" name="myfile" id="fileToUpload" required>
            </div>
            <br>
            <button class="btn btn-primary btn-block" name="submit" type="submit">Publish Post</a>
          </form>
        </div>
      </div>
    </div>

<!--CONTENT HERE END -->