<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->
<?php
     $idtoedit =$_GET['id'];
     require 'dbconnection.php';
        
$queryforedituser = mysqli_query($databaseconnection,"SELECT * FROM schedule WHERE sched_id =$idtoedit");
$fetch = mysqli_fetch_assoc($queryforedituser);
?>

<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">EDIT SCHEDULE</div>
        <div class="card-body">
          <form method="post" action="adminscheduleeditprocess.php">
          <input hidden name="id" value="<?php echo $fetch['sched_id']; ?>">
          <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    Time In
                    <input value="<?php echo $fetch['time_in']; ?>" type="time" name="timein" id="username" class="form-control" placeholder="Announcement Title" required="required">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    Time Out
                    <input value="<?php echo $fetch['time_out']; ?>" type="time" name="timeout" id="username" class="form-control" placeholder="Announcement Title" required="required">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                  Remarks
                    <textarea  type="text" name="remarks"  class="form-control"  required="required"><?php echo$fetch['description'];?><?php echo $fetch['remarks']; ?></textarea>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <button class="btn btn-primary btn-block" name="submit" type="submit">Update Schedule</a>
          </form>
        </div>
      </div>
    </div>
   
