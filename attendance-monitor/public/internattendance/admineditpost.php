<?php
session_start();
error_reporting(0);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->
<?php
     $idtoedit =$_GET['id'];
     require 'dbconnection.php';
        
$queryforedituser = mysqli_query($databaseconnection,"SELECT * FROM post WHERE post_id =$idtoedit");
$fetch = mysqli_fetch_assoc($queryforedituser);
?>

<div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">EDIT AN Announcement</div>
        <div class="card-body">
          <form method="post" action="admineditpostprocess.php" enctype="multipart/form-data">
          <input hidden name="id" value="<?php echo $fetch['post_id']; ?>">
          <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                  Announcement Title
                    <input type="text" name="posttitle" id="username" class="form-control" placeholder="Announcement Title" required="required" value="<?php echo $fetch['name']; ?>">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                  Announcement Message 
                    <textarea  type="text" name="postvalue"  class="form-control"  required="required"><?php echo$fetch['description'];?></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
            Select image to upload:
            <img  style="width: 250px !;
    height: 250px !important;" src="<?php echo "internattendance/.." .$fetch['photo'];  ?>" id="blah" src="#">
    <input type="text" hidden name="edituploadpathhidden" value = "<?php echo $fetch['photo'];  ?>">
             <input type="file" name="myfile" id="fileToUpload"  onchange="readURL(this);">
            </div>
            <br>
            <button class="btn btn-primary btn-block" name="submit" type="submit">Publish Post</a>
          </form>
        </div>
      </div>
    </div>
   
    <script>

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(250);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
<!--CONTENT HERE END -->