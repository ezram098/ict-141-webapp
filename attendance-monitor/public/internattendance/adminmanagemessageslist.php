<?php
session_start();
error_reporting(0);
$curdate = time();
$curdate = date("y-m-d h:i",$curdate);
require 'adminsession.php';
?>
<?php
include('header.php');
?>
<!-- CONTENT HERE START-->

 <!-- DataTables Example -->
 <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-comments"></i>
              Mail &nbsp;&nbsp; 
              </div>
            <div class="card-body">
              <div class="table-responsive">
<?php
require 'dbconnection.php';
 $getalluser = "
 SELECT *
 FROM message_person mp
 JOIN person p ON mp.person_id = p.person_id
 ";
if($outputresult = mysqli_query($databaseconnection, $getalluser)){
    if(mysqli_num_rows($outputresult) > 0){
?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Sender Name</th>
                      <th>Message Title</th>
                      <th>Body</th>
                      <th>Sent Date</th>
                      <th>Reply</th>
                      <th>Manage</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
            while($row = mysqli_fetch_array($outputresult)){
    
                echo "<tr>";
                echo "<td>" .$row['fname']."&nbsp;".$row['lname']."</td>";
                echo "<td>" .$row['title']."</td>";
                echo "<td>" .$row['description']."</td>";
                echo "<td>" .$row['created']."</td>";
                echo "<td>" .$row['reply']."</td>";
                echo "<td> <a href='adminreplytomessage.php?id={$row['messageperson_id']}'><i class='fas fa-envelope'></i> REPLY</a>
                &nbsp;
                </td>";
                echo "</tr>";
        }
    }
}?>
                   
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Last updated at <?php echo $curdate; ?></div>
          </div>


<!--CONTENT HERE END -->
<?php
include('footer.php');
?>
 