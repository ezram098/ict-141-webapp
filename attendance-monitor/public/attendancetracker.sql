-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2018 at 03:08 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `attendancetracker`
--
CREATE DATABASE IF NOT EXISTS `attendancetracker` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `attendancetracker`;

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `attendance_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `remarksattendance` text,
  `person_id` int(11) NOT NULL,
  `sched_id` int(11) NOT NULL,
  `isinisout` tinyint(1) NOT NULL,
  `timelog` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`attendance_id`, `date`, `remarksattendance`, `person_id`, `sched_id`, `isinisout`, `timelog`) VALUES
(27, '2018-12-14', '', 4, 1, 1, '00:00:00'),
(28, '2018-12-14', '', 3, 1, 1, '00:00:00'),
(35, '2018-12-14', '', 5, 1, 1, '00:00:00'),
(36, '2018-12-15', '', 6, 0, 1, '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `person_id`, `description`) VALUES
(17, 2, 'INSERT:test,test,123,asa@gmail.com,2018-12-14,tete,test,test'),
(18, 4, 'INSERT:OJT concern, hi gwapo,,2018/12/12'),
(19, 2, 'UPDATE: 1,<br>12/12/2018:  hello'),
(20, 4, 'INSERT:work work wok, work ko ron?,,2018/12/12'),
(21, 2, 'INSERT:tets,            stet       ,2,2018/12/13'),
(22, 2, 'INSERT:tinarung,test tarung,2,2018/12/13'),
(23, 2, 'UPDATE: sundalo,zzzz            ,2018/12/13,2'),
(24, 2, 'UPDATE: vbvbcvbcvb, qwrdfsdf,2018/12/13,2'),
(25, 2, 'UPDATE: asdcvxcvxcv,xcvxcvxcv            ,2018/12/13,2'),
(26, 2, 'UPDATE: gerger,ger,2018/12/13,2'),
(27, 2, 'UPDATE: gerger,ger,2018/12/13,2'),
(28, 2, 'UPDATE: gerger,ger,2018/12/13,2'),
(29, 2, 'UPDATE: zzzzzzzzzzzzzz,asdasdasd         ,2018/12/13,2'),
(30, 2, 'UPDATE: zzzzzzzzzzzzzz,asdasdasdasdasdasd             ,2018/12/13,2'),
(31, 2, 'UPDATE: zzzzzzzzzzzzzz,                    asdasdasdasdasdasd                                 ,2018/12/13,2'),
(32, 2, 'UPDATE: zzzzzzzzzzzzzz, asdasdasdasdasdasd                                                     ,2018/12/13,2'),
(33, 2, 'UPDATE: zzzzzzzzzzzzzz,asdasdasd                                                               ,2018/12/13,2'),
(34, 2, 'UPDATE: zzzzzzzzzzzzzz,cvbcvbcvb                          ,2018/12/13,2'),
(35, 2, 'UPDATE: zzzzzzzzzzzzzz,test,2018/12/13,2'),
(36, 2, 'INSERT:cvx,                    xcv,2,2018/12/13'),
(37, 2, 'INSERT:bcvb,asdasd,2,2018/12/13'),
(38, 2, 'INSERT:cvbc,                    bvcb,2,2018/12/13'),
(39, 2, 'INSERT:vbcvbc,                    vbcvb,2,2018/12/13'),
(40, 2, 'INSERT:08:00,17:00,testing,2018/12/13'),
(41, 2, 'UPDATE: 1,07:00,16:00,2018/12/13,bnvbnvbnvbn,2'),
(42, 2, 'UPDATE: 2,2018-12-14,08:00,2018/12/13,test,0,2'),
(43, 2, 'UPDATE: 2,2018-12-14,08:00,2018/12/13,test,1,2'),
(44, 2, 'UPDATE: 2,2018-12-14,20:00,2018/12/13,test out,1,2'),
(45, 2, 'UPDATE: 2,2018-12-14,20:00,2018/12/13,test outasdasd,0,2'),
(46, 2, 'UPDATE: ,12:00,18:22,2018/12/14,wa sas,2'),
(47, 2, 'UPDATE: 2,2018-12-19,06:10,2018/12/14,,0,2'),
(48, 2, 'UPDATE: 4,2018-12-14,13:59,2018/12/14,out,1,2'),
(49, 2, 'UPDATE: 3,2018-12-15,07:30,2018/12/14,kapoy,0,2'),
(50, 2, 'UPDATE: 3,2018-12-14,07:30,2018/12/14,,0,2'),
(51, 2, 'UPDATE: 3,2018-12-14,13:00,2018/12/14,,1,2'),
(52, 2, 'UPDATE: 3,2018-12-21,16:25,2018/12/14,,1,2'),
(53, 2, 'UPDATE: 4,2018-12-14,01:00,2018/12/14,,0,2'),
(54, 2, 'UPDATE: 4,2018-12-14,15:33,2018/12/14,,1,2'),
(55, 2, 'UPDATE: 3,2018-12-14,07:03,2018/12/14,,0,2'),
(56, 2, 'UPDATE: 4,2018-12-14,01:02,2018/12/14,,1,2'),
(57, 2, 'UPDATE: 2,2018-12-14,13:02,2018/12/14,,1,2'),
(58, 2, 'UPDATE: 4,2018-12-15,02:02,2018/12/14,,1,2'),
(59, 2, 'UPDATE: 2,2018-12-14,14:03,2018/12/14,,1,2'),
(60, 2, 'UPDATE: 2,2018-12-14,01:03,2018/12/14,,0,2'),
(61, 2, 'UPDATE: 1,07:00:00,16:00:00,2018/12/14,monday to friday\r\n,2'),
(62, 2, 'INSERT:gg,                    wasa,2,2018/12/14'),
(63, 2, 'UPDATE: 2,09:00:00,18:00:00,2018/12/14,TTh,2'),
(64, 2, 'UPDATE: 2,09:00:00,18:00:00,2018/12/14,Saturday,2'),
(65, 2, 'UPDATE: 2,2018-12-15,01:03,2018/12/14,,1,2'),
(66, 2, 'UPDATE: 3,2018-12-15,02:03,2018/12/14,,1,2'),
(67, 2, 'UPDATE: 3,2018-12-20,01:03,2018/12/14,,1,2'),
(68, 2, 'UPDATE: ,02:03,03:03,2018/12/14,sdasdad,2'),
(69, 2, 'UPDATE: 2,2018-12-23,,2018/12/14,wa ka mata,0,2'),
(70, 2, 'UPDATE: 2,2018-12-29,,2018/12/14,,1,2'),
(71, 4, 'INSERT:gfgfd, gfghghjhh,,2018/12/14'),
(72, 2, 'UPDATE: 2,<br>12/14/2018:  oo'),
(73, 2, 'UPDATE: 3,<br>12/14/2018:  oo'),
(74, 2, 'UPDATE: 2,10:00,19:00,2018/12/14,Saturday,2'),
(75, 2, 'UPDATE: 1,08:00,17:00,2018/12/14,monday to friday\r\n,2'),
(76, 2, 'INSERT:ezra,martinezz,09294818820,ezram098@gmail.com,2018-12-12,BS - ICT,ezram,123456'),
(77, 2, 'UPDATE: 5,2018-12-14,,2018/12/14,,1,2'),
(78, 2, 'UPDATE: 5,2018-12-14,,2018/12/14,,1,2'),
(79, 2, 'UPDATE: 4,2018-12-14,,2018/12/14,,1,2'),
(80, 2, 'UPDATE: 4,2018-12-14,,2018/12/14,,1,2'),
(81, 2, 'UPDATE: 3,2018-12-14,,2018/12/14,,1,2'),
(82, 2, 'UPDATE: 5,2018-12-14,,2018/12/14,,1,2'),
(83, 5, 'INSERT:attendance,di ka punch in\r\n ,,2018/12/14'),
(84, 2, 'UPDATE: 5,2018-12-15,,2018/12/14,,1,2'),
(85, 2, 'UPDATE: 5,2018-12-14,,2018/12/14,,0,2'),
(86, 2, 'INSERT:donald,trump,1231231,donald@gmail.com,2018-07-19,BS - ICT,donald,123456'),
(87, 2, 'UPDATE: 6,2018-12-14,,2018/12/14,,1,2'),
(88, 2, 'UPDATE: ezra,martinezz,2018-12-12,ezram098@gmail.com,BS - ICT,ezram,2,09294818820'),
(89, 2, 'UPDATE: 5,2018-12-14,,2018/12/14,,1,2'),
(90, 2, 'UPDATE: 7,2018-12-14,,2018/12/14,,1,2'),
(91, 2, 'UPDATE: 5,2018-12-14,,2018/12/14,,1,2'),
(92, 2, 'UPDATE: christian,martinez,2018-12-06,chris@gmail.com,bs-ict,chris,1,123344'),
(93, 2, 'UPDATE: christian,martinez,2018-12-06,chris@gmail.com,bs-ict,chris,1,123344'),
(94, 2, 'INSERT:08:00,05:00,m-f,2018/12/14'),
(95, 2, 'UPDATE: 3,07:00,16:00,2018/12/14,m-f,2'),
(96, 2, 'UPDATE: 6,2018-12-15,,2018/12/14,,1,2'),
(97, 2, 'UPDATE: donald,trump,2018-07-19,donald@gmail.com,BS - ICT,donald,1,1231231');

-- --------------------------------------------------------

--
-- Table structure for table `message_person`
--

CREATE TABLE `message_person` (
  `messageperson_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created` date NOT NULL,
  `isread` tinyint(4) NOT NULL,
  `isreads` date NOT NULL,
  `reply` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_person`
--

INSERT INTO `message_person` (`messageperson_id`, `person_id`, `title`, `description`, `created`, `isread`, `isreads`, `reply`) VALUES
(1, 4, 'OJT concern', ' hi gwapo', '2018-12-12', 1, '0000-00-00', '<br>12/12/2018:  hello'),
(2, 4, 'work work wok', ' work ko ron?', '2018-12-12', 1, '0000-00-00', '<br>12/14/2018:  oo'),
(3, 4, 'gfgfd', ' gfghghjhh', '2018-12-14', 1, '0000-00-00', '<br>12/14/2018:  oo'),
(4, 5, 'attendance', 'di ka punch in\r\n ', '2018-12-14', 0, '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `person_id` int(11) NOT NULL,
  `usertype` enum('intern','admin') NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `bdate` date NOT NULL,
  `course` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `islocked` tinyint(4) NOT NULL,
  `sched_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `usertype`, `fname`, `lname`, `contact`, `email`, `bdate`, `course`, `username`, `password`, `islocked`, `sched_id`) VALUES
(2, 'admin', 'chelo', 'arbuis', '69', 'chelo@gmail.com', '1995-12-24', 'ICT', 'chelo', 'chelo', 0, 1),
(3, 'intern', 'sad', 'sad', '222', 'aaa@gmail.com', '2013-12-05', 'ict', 'sad', 'sad', 0, 1),
(4, 'intern', 'test', 'test', '123', 'asa@gmail.com', '2018-12-14', 'tete', 'test', 'test', 0, 1),
(5, 'admin', 'ezra', 'martinezz', '09294818820', 'ezram098@gmail.com', '2018-12-12', 'BS - ICT', 'ezram', 'admin123', 0, 1),
(6, 'intern', 'donald', 'trump', '1231231', 'donald@gmail.com', '2018-07-19', 'BS - ICT', 'donald', 'trump', 0, 0),
(7, 'intern', 'christian', 'martinez', '123344', 'chris@gmail.com', '2018-12-06', 'bs-ict', 'chris', '123456', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `photo` text NOT NULL,
  `description` text NOT NULL,
  `createdby` varchar(50) NOT NULL,
  `createddate` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `isdeactivated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`post_id`, `name`, `photo`, `description`, `createdby`, `createddate`, `person_id`, `isdeactivated`) VALUES
(3, 'cvx', '/fileuploads/SmJpcB0.jpg', '                    xcv', '2', '2018-12-13', 2, 0),
(4, 'bcvb', '/fileuploads/39095625_2281547742074558_1808996901740085248_n.jpg', 'asdasd', '2', '2018-12-13', 2, 0),
(5, 'cvbc', '/fileuploads/39651723_218906175649700_825498838914564096_n.jpg', '                    bvcb', '2', '2018-12-13', 2, 0),
(6, 'vbcvbc', '/fileuploads/31102396_2224991700875093_1106803447157489664_n.jpg', '                    vbcvb', '2', '2018-12-13', 2, 1),
(7, 'gg', '/fileuploads/IMG_1214-2.jpg', '                    wasa', '2', '2018-12-14', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `sched_id` int(11) NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL,
  `remarks` text NOT NULL,
  `lockedSched` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`sched_id`, `time_in`, `time_out`, `remarks`, `lockedSched`) VALUES
(0, '07:00:00', '16:00:00', 'm-f', 0),
(1, '08:00:00', '17:00:00', 'monday to friday\r\n', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`attendance_id`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `sched_id` (`sched_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `message_person`
--
ALTER TABLE `message_person`
  ADD PRIMARY KEY (`messageperson_id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `sched_id` (`sched_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `person_id_2` (`person_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`sched_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `message_person`
--
ALTER TABLE `message_person`
  MODIFY `messageperson_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `sched_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`),
  ADD CONSTRAINT `attendance_ibfk_2` FOREIGN KEY (`sched_id`) REFERENCES `schedule` (`sched_id`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`);

--
-- Constraints for table `message_person`
--
ALTER TABLE `message_person`
  ADD CONSTRAINT `message_person_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
