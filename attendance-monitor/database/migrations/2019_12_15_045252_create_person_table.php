<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person', function (Blueprint $table) {
            $table->increments('person_id');
            $table->enum('usertype', ['intern','admin']);
            $table->string('fname');
            $table->string('lname');
            $table->string('contact');
            $table->string('email');
            $table->date('bdate');
            $table->string('course');
            $table->int('year');
            $table->string('username');
            $table->string('password');
            $table->tinyint('islocked');
            $table->int('sched_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person');
    }
}
